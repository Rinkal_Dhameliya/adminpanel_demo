@extends('user.auth.layout')

@section('title')
  User Reset Password | Project
@stop

@section('content')

<div class="main-page login-page ">
	<h2 class="title1">User Reset Password</h2>
	<div class="widget-shadow">
		<div class="login-body">
			<form action="{{ route('post:user:reset:password', $user->id) }}" method="post">
				{{ csrf_field() }}

				@if(\Session::has('error'))
                <div class="row">
                    <div class="alert alert-danger col-md-12">{{ \Session::get('error') }}</div>
                </div>
            	@endif

	            <div class="input-with-icon right">
					<input type="password" name="password" class="lock" placeholder="Password">
					<span class="text-danger">{{ $errors->first('password') }}</span>
				</div>

				<div class="input-with-icon right">
					<input type="password" name="confirm_password" class="lock" placeholder="Confirm Password">
					<span class="text-danger">{{ $errors->first('confirm_password') }}</span>
				</div>

				<div class="row">
	                <div class="col-md-12">
	                    <button class="btn btn-primary btn-cons btn-block" type="submit">Reset Password</button>
	                </div>
	            </div>
			</form>
		</div>
	</div>
</div>

@stop
