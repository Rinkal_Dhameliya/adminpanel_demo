<!DOCTYPE HTML>
<html>
<head>
<title>@yield('title')</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="{{ asset('/') }}admin/css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="{{ asset('/') }}admin/css/style.css" rel='stylesheet' type='text/css' />
<link href="{{ asset('/') }}admin/css/font-awesome.css" rel="stylesheet"> 
<link href="{{ asset('/') }}admin/css/SidebarNav.min.css" media='all' rel='stylesheet' type='text/css'/>
<script src="{{ asset('/') }}admin/js/jquery-1.11.1.min.js"></script>
<script src="{{ asset('/') }}admin/js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts-->
 
<script src="{{ asset('/') }}admin/js/metisMenu.min.js"></script>
<script src="{{ asset('/') }}admin/js/custom.js"></script>
<link href="{{ asset('/') }}admin/css/custom.css" rel="stylesheet">
@yield('page-css')
</head> 
<body>
<div class="main-content">
	<div id="page-wrapper">
		 @yield('content')
	</div>
</div>
<script src="{{ asset('/') }}admin/js/scripts.js"></script>
<script src="{{ asset('/') }}admin/js/bootstrap.js"> </script>
@yield('page-js')
</body>
</html>