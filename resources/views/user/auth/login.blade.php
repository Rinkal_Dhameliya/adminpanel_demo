@extends('user.auth.layout')

@section('title')
    User Login | Project
@stop

@section('page-css')
	<style type="text/css">
		/*#page-wrapper{
			padding-top: 5%;
		}*/

	</style>
@stop
@section('content')
		<div class="main-page login-page">
			<h2 class="title1">User Login</h2>
			<div class="widget-shadow">
				<div class="login-body">
					<form action="{{ route('post:user:login') }}" method="post">
						{{ csrf_field() }}

						@if(\Session::has('error'))
	                		<div class="row">
	                    		<div class="alert alert-danger col-md-12">{{ \Session::get('error') }}</div>
	                		</div>
            			@endif
			            @if(\Session::has('success'))
			                <div class="row">
			                    <div class="alert alert-success col-md-12">{{ \Session::get('success') }}</div>
			                </div>
			            @endif

			            <div class="input-with-icon right">
							<input type="email" class="user" name="email" placeholder="Enter Your Email">
							<span class="text-danger">{{ $errors->first('email') }}</span>
						</div>

						<div class="input-with-icon right">
							<input type="password" name="password" class="lock" placeholder="Password">
							<span class="text-danger">{{ $errors->first('password') }}</span>
						</div>

						<div class="forgot-grid">
							<div class="pull-left">
								<a href="{{ route('post:user:register') }}">Sign Up</a>
							</div>
							<div class="forgot pull-right">
								<a href="{{ route('user:forgot:password') }}">forgot password?</a>
							</div>
							<div class="clearfix"> </div>
						</div>
						<input type="submit" name="Sign In" value="Log In" class="pull-left">
						<a href="{{ route('user:login:redirect') }}" class="btn btn-primary pull-right google-btn">{{ __('Google Sign in') }}</a>
					</form>
				</div>
			</div>
		</div>
@stop
