@extends('user.auth.layout')

@section('title')
    User Login | Project
@stop

@section('page-css')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">

	<style type="text/css">
		#page-wrapper{
			padding-top: 1%;
		}

	</style>
@stop

@section('content')
		<div class="main-page login-page">
			<h2 class="title1">Register</h2>
			<div class="widget-shadow">
				<div class="login-body">
					<form action="{{ route('post:user:register') }}" method="post">
						{{ csrf_field() }}

						@if(\Session::has('error'))
	                		<div class="row">
	                    		<div class="alert alert-danger col-md-12">{{ \Session::get('error') }}</div>
	                		</div>
            			@endif
			            @if(\Session::has('success'))
			                <div class="row">
			                    <div class="alert alert-success col-md-12">{{ \Session::get('success') }}</div>
			                </div>
			            @endif

			            <div class="input-with-icon right">
							<input type="text" class="user" name="name" placeholder="Name">
							<span class="text-danger">{{ $errors->first('name') }}</span>
						</div>

			            <div class="input-with-icon right">
							<input type="email" class="user" name="email" placeholder="Enter Your Email">
							<span class="text-danger">{{ $errors->first('email') }}</span>
						</div>	

						<div class="input-with-icon right">	
							<input type="password" name="password" class="lock" placeholder="Password">
							<span class="text-danger">{{ $errors->first('password') }}</span>
						</div>

						 <div class="input-with-icon right">
                                {{
                                    html()
                                    ->input('text', 'birth_date')
                                    ->class('datepicker')
                                    ->placeholder('yyyy-mm-dd')
                                    }}
                                <span class="text-danger">{{ $errors->first('birth_date') }}</span>
                            </div>
						
						<input type="submit" name="Sign In" value="Sign In">	
					</form>
				</div>
			</div>	
		</div>
@stop

@section('page-js')
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>

    <script>
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: false
        });
    </script>
@stop