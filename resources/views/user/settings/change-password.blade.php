@extends('user.layout.index')

@section('title')
    User Change Password
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="grid simple">
                <div class="grid-title no-border">
                    <h3>User Change Password</h3>
                </div>
                <div class="grid-body no-border">
                    <div class="row">
                        <div class="col-xs-12">

                            {{ html()->form('POST', route('post:user:change:password'))->open() }}
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="">Current Password</label>
                                {{
                                    html()
                                        ->password('current_password')
                                        ->class('form-control')
                                        ->placeholder('********')
                                }}
                                <span class="error">{{ $errors->first('current_password') }}</span>
                            </div>

                            <div class="form-group">
                                <label for="">New Password</label>
                                {{
                                    html()
                                        ->password('password')
                                        ->class('form-control')
                                        ->placeholder('********')
                                }}
                                <span class="error">{{ $errors->first('password') }}</span>
                            </div>

                            <div class="form-group">
                                <label for="">Confirm New Password</label>
                                {{
                                    html()
                                        ->password('confirm_password')
                                        ->class('form-control')
                                        ->placeholder('********')
                                }}
                                <span class="error">{{ $errors->first('confirm_password') }}</span>
                            </div>

                            <div class="form-group">
                                <span class="edit-cancel-button-append"></span>
                                <button class="btn btn-primary" type="submit">Change Password</button>
                            </div>
                            {{ html()->form()->close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
