<aside class="sidebar-left">
  <nav class="navbar navbar-inverse">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <div class="navbar-brand"><h1>User Data</h1></div>
      </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="sidebar-menu">
          <li class="treeview {{ request()->is('user/dashboard*') ? 'active' : '' }}">
            <a href="{{ route('user:dashboard') }}">
            <i class="fa fa-home"></i> <span>Dashboard</span>
            </a>
          </li>
          <li class="treeview {{ request()->is('user/book*') ? 'active' : '' }}">
            <a href="{{ url('user/book') }}">
            <i class="fa fa-book"></i>
            <span>Book</span>
            </a>
          </li>
          <li class="treeview {{ request()->is('user/issue-books*') ? 'active' : '' }}">
            <a href="{{ url('user/issue-books') }}">
            <i class="fa fa-book"></i>
            <span>Issue Book</span>
            </a>
          </li>
          
        </ul>
      </div>
  </nav>
</aside>