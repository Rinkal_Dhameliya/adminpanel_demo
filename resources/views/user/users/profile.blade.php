@extends('user.layout.index')

@section('title')
    User Profile
@stop

@section('page-css')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="grid simple">
            <div class="grid-title no-border">
                <h3>Edit User Profile</h3>
            </div>
            <div class="grid-body no-border">
                <div class="row">
                    <div class="col-xs-12">
                        {{ html()->form('POST', route('userprofile.store'))->open() }}
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="">Name</label>
                            {{
                                html()
                                    ->text('name', old('name', $user->name))
                                    ->class('form-control')
                                    ->placeholder('Name')
                            }}
                            <span class="error">{{ $errors->first('name') }}</span>
                        </div>

                        <div class="form-group">
                            <label for="">Email</label>
                            {{
                                html()
                                    ->email('email', old('email', $user->email))
                                    ->class('form-control')
                                    ->placeholder('Email')
                            }}
                            <span class="error">{{ $errors->first('email') }}</span>
                        </div>

                        <div class="form-group">
                            <label for="">Birth Date</label>
                            <div class="input-with-icon right">
                                {{
                                    html()
                                    ->text('birth_date', old('birth_date', $user->birth_date))
                                    ->class('form-control datepicker')
                                    ->placeholder('yyyy-mm-dd')
                                    }}
                                <span class="text-danger">{{ $errors->first('birth_date') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <span class="edit-cancel-button-append"></span>
                            <button class="btn btn-primary" type="submit">Update User  Profile</button>
                        </div>
                        {{ html()->form()->close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('page-js')
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>

   
    <script>
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: false
        });
    </script>
@stop
