@extends('user.layout.index')

@section('title')
    Book List
@stop

@section('page-css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
@stop

@section('content')

<div class="page-title">
    <h3>Manage Book</h3>
</div>

<div class="row">
        <div class="col-md-12">
            <div class="grid simple">
                <div class="grid-body no-border">
                    {!! $html->table() !!}
                </div>
            </div>
        </div>
    </div>
@stop


@section('page-js')

<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    {!! $html->scripts() !!}
@stop