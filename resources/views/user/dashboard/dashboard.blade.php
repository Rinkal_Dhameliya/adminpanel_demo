@extends('user.layout.index')

@section('title')
   User Dashboard
@stop

@section('page-css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
@stop

@section('content')

<!-- main content start-->
<div class="col_3">
  <div class="col-md-3 widget widget1">
    <div class="r3_counter_box">
      <i class="pull-left fa fa-book user1 icon-rounded"></i>
      <div class="stats">
        <h5><strong>Total Book</strong></h5>
        <span data-value="{{ $totalBooks }}">{{ $totalBooks }}</span>
      </div>
    </div>
  </div>
  <div class="col-md-3 widget widget1">
    <div class="r3_counter_box">
      <i class="pull-left fa fa-user icon-rounded"></i>
      <div class="stats">
        <h5><strong>Total Issue Book</strong></h5>
        <span data-value="{{ $total_issuebook }}">{{ $total_issuebook }}</span>
      </div>
    </div>
  </div>
</div>

<div class="clear" style="padding-top:13%"></div>

  <div class="col-md-6 pull-left">
    <div class="panel panel-default">
      <div class="panel-heading">Total Issue Book <span class="pull-right">{{ $format}}</span></div>
        <div class="panel-body">
          <table class="table table-hover table-bordered table-striped datatable" style="width:100%">
            <thead>
              <tr>
                  <th>Id</th>
                  <th>User Name</th> 
                  <th>Book Name</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
  </div>

 <div class="col-md-6 pull-right">
    <div class="grid simple">
        <div class="grid-body no-border">
          <div class="pull-left head-list">List Of Today's Renew Book</div>
          <div class="pull-right head-date">{{ $format}}</div>
            {!! $html->table() !!}
        </div>
    </div>
</div>
@stop

@section('page-js')

<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    {!! $html->scripts() !!}

<script type="text/javascript">
  $(document).ready(function() {
    $('.datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ route('dashboard-user-second:postdata') }}',
        type: "GET",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'user.name', name: 'user_id'},
            {data: 'book.book_name', name: 'book_id'},
        ]
    });
});
</script>
@stop