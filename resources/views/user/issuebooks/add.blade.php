@extends('user.layout.index')

@section('title')
    Add Issue Book
@stop

@section('page-css')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="grid simple">
            <div class="grid-title no-border">
                <h4>Add Issue Book</h4><br>
            </div>
            <div class="grid-body no-border">
                <div class="row">
                    <div class="col-xs-12">
                        {{ html()->form('POST', route('issue-books.store'))->acceptsFiles()->open() }}
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="">User Name</label>
                            <input type="hidden" name="user_id" id="user_id" value="{{auth('web')->user()->id}}">
                            {{
                                html()
                                    ->text('user', $users)
                                    ->attribute('value', auth('web')->user()->name)
                                    ->id('user')
                                    ->attribute('disabled',true)
                                    ->class('form-control')
                            }}
                            <span class="text-danger">{{ $errors->first('user') }}</span>
                        </div>
                        
                        <div class="form-group">
                            <label for="">Book Name</label>
                            {{
                                html()
                                    ->select('book', $books)
                                    ->id('book')
                                    ->class('form-control')
                            }}
                            <span class="text-danger">{{ $errors->first('book') }}</span>
                        </div>

                        <div class="form-group">
                            <label for="">Issue Date</label>
                            <div class="input-with-icon right">
                                {{
                                    html()
                                    ->input('text', 'issue_date')
                                    ->class('form-control datepicker')
                                    ->placeholder('yyyy-mm-dd')
                                    }}
                                <span class="text-danger">{{ $errors->first('issue_date') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="">Expiry Date</label>
                            <div class="input-with-icon right">
                                {{
                                    html()
                                    ->input('text', 'expiry_date')
                                    ->class('form-control datepicker')
                                    ->placeholder('yyyy-mm-dd')
                                    }}
                                <span class="text-danger">{{ $errors->first('expiry_date') }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <span class="edit-cancel-button-append"></span>
                            <button class="btn btn-primary" type="submit">Issue Book</button>
                            <a class="btn btn-default" href="{{ route('issuebooks.index') }}">Cancel</a>
                        </div>
                        {{ html()->form()->close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('page-js')
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>

   
    <script>
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: false
        });
    </script>
@stop
