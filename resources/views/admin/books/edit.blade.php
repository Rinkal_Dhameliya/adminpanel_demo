@extends('admin.layout.index')

@section('title')
    Edit Book
@stop

@section('page-css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
@stop

@section('content')
    <div class="page-title">
        <h3>Edit Book</h3>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="grid simple">
                <div class="grid-body no-border">
                    
                {{ html()->form('PUT', route('books.update', $book->getKey()))->open() }}
                {{ csrf_field() }}
                <div class="row">
                   
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label">Book Name</label>
                            {{
                                html()
                                    ->input('text', 'book_name', old('book_name', $book->book_name))
                                    ->class('form-control')
                                    ->placeholder('Book Name')
                            }}
                            <span class="error">{{ $errors->first('book_name') }}</span>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label">Author Name</label>
                            {{
                                html()
                                    ->input('text', 'author_name', old('author_name', $book->author_name))
                                    ->class('form-control')
                                    ->placeholder('Author Name')
                            }}
                            <span class="error">{{ $errors->first('author_name') }}</span>
                        </div>
                    </div>


                     <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label">Category</label>
                            <div class="droplist">
                                {{
                                html()
                                    ->select('category',$categorys, old('category', $book->category)  )
                                    ->class('form-control')
                            }}
                                <span class="error">{{ $errors->first('category') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label">Price</label>
                            {{
                                html()
                                    ->input('text', 'price', old('price', $book->price))
                                    ->class('form-control')
                                    ->placeholder('Price')
                            }}
                            <span class="error">{{ $errors->first('price') }}</span>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <button class="btn btn-primary" type="submit">Edit Book</button>
                            <a href="{{route('books.index')}}" class="btn btn-white">Cancel</a></div>
                    </div>
                </div>

                {{ html()->form()->close() }}
            </div>
            </div>
        </div>
    </div>
@stop
