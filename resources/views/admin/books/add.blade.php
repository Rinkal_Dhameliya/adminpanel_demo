@extends('admin.layout.index')

@section('title')
    Add Book
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="grid simple">
                <div class="grid-title no-border">
                    <h4>Add Book</h4><br>
                </div>
                <div class="grid-body no-border">
                    <div class="row">
                        <div class="col-xs-12">
                            {{ html()->form('POST', route('books.store'))->acceptsFiles()->open() }}
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="">Book Name</label>
                                    {{
                                        html()
                                            ->input('text', 'book_name', old('book_name'))
                                            ->class('form-control')
                                            ->placeholder('Book Name')
                                    }}
                                    <span class="error">{{ $errors->first('book_name') }}</span>
                                </div>

                                <div class="form-group">
                                    <label for="">Author Name</label>
                                    {{
                                        html()
                                            ->input('text', 'author_name', old('author_name'))
                                            ->class('form-control')
                                            ->placeholder('Author Name')
                                    }}
                                    <span class="error">{{ $errors->first('author_name') }}</span>
                                </div>

                                <div class="form-group">
                                    <label for="">Category</label>
                                    {{
                                        html()
                                            ->select('category', $categorys)
                                            ->id('category')
                                            ->class('form-control')
                                    }}
                                   
                                    <span class="error">{{ $errors->first('category') }}</span>
                                </div>

                                <div class="form-group">
                                    <label for="">Price</label>
                                    {{
                                        html()
                                            ->input('text', 'price', old('price'))
                                            ->class('form-control')
                                            ->placeholder('Price')
                                    }}
                                    <span class="error">{{ $errors->first('price') }}</span>
                                </div>
                               
                                <div class="form-group">
                                    <span class="edit-cancel-button-append"></span>
                                    <button class="btn btn-primary" type="submit">Add Book</button>
                                    <a class="btn btn-default" href="{{ route('books.index') }}">Cancel</a>
                                </div>
                            {{ html()->form()->close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
