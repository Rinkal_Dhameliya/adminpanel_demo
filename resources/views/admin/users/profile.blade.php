@extends('admin.layout.index')

@section('title')
    Profile
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="grid simple">
                <div class="grid-title no-border">
                    <h3>Edit Profile</h3>
                </div>
                <div class="grid-body no-border">
                    <div class="row">
                        <div class="col-xs-12">
                            {{ html()->form('POST', route('profile.store'))->open() }}
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="">Name</label>
                                    {{
                                        html()
                                            ->text('name', old('name', $user->username))
                                            ->class('form-control')
                                            ->placeholder('Name')
                                    }}
                                    <span class="error">{{ $errors->first('name') }}</span>
                                </div>

                                <div class="form-group">
                                    <label for="">Email</label>
                                    {{
                                        html()
                                            ->email('email', old('email', $user->email))
                                            ->class('form-control')
                                            ->placeholder('Email')
                                    }}
                                    <span class="error">{{ $errors->first('email') }}</span>
                                </div>

                                <div class="form-group">
                                    <span class="edit-cancel-button-append"></span>
                                    <button class="btn btn-primary" type="submit">Update Profile</button>
                                </div>
                            {{ html()->form()->close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
