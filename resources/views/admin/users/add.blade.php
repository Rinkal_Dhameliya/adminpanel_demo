@extends('admin.layout.index')

@section('title')
    Add User
@stop

@section('page-css')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="grid simple">
            <div class="grid-title no-border">
                <h4>Add New User</h4><br>
            </div>
            <div class="grid-body no-border">
                <div class="row">
                    <div class="col-xs-12">

                        {{ html()->form('POST', route('users.store'))->acceptsFiles()->open() }}
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="">Name</label>
                                {{
                                    html()
                                        ->input('text', 'name', old('name'))
                                        ->class('form-control')
                                        ->placeholder('User Name')
                                }}
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            </div>

                            <div class="form-group">
                                <label for="">Email</label>
                                {{
                                    html()
                                        ->input('text', 'email', old('email'))
                                        ->class('form-control')
                                        ->placeholder('Email')
                                }}
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                            </div>

                            <div class="form-group"><label for="">Birth Date</label>
                                {{
                                    html()
                                    ->input('text', 'birth_date')
                                    ->class('form-control datepicker')
                                    ->placeholder('yyyy-mm-dd')
                                    }}
                                <span class="text-danger">{{ $errors->first('birth_date') }}</span>
                            </div>

                            <div class="form-group">
                                <span class="edit-cancel-button-append"></span>
                                <button class="btn btn-primary" type="submit">Add User</button>
                                <a class="btn btn-default" href="{{ route('users.index') }}">Cancel</a>
                            </div>
                        {{ html()->form()->close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('page-js')

    <script>
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: false
        });
    </script>
@stop
