@extends('admin.layout.index')

@section('title')
    Edit User
@stop

@section('page-css')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">
@stop

@section('content')
    <div class="page-title">
        <h3>Edit User</h3>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="grid simple">
                <div class="grid-body no-border">
                    @include('admin.shared.messages')

                    {{ html()->form('POST', route('users.update', $user->getKey()))->open() }}
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="form-group">
                            <label for="">Name</label>
                            {{
                                html()
                                    ->input('text', 'name', old('name', $user->name))
                                    ->class('form-control')
                                    ->placeholder('Name')
                            }}
                            <span class="error">{{ $errors->first('name') }}</span>
                        </div>

                        <div class="form-group">
                            <label for="">Email</label>
                            {{
                                html()
                                    ->input('email', 'email', old('email', $user->email))
                                    ->class('form-control')
                                    ->placeholder('Email')
                            }}
                            <span class="error">{{ $errors->first('email') }}</span>
                        </div>

                        <div class="form-group">
                            <label for="">Birth Date</label>
                             {{
                                    html()
                                    ->input('text', 'birth_date', old('birth_date', $user->birth_date))
                                    ->class('form-control datepicker')
                                    ->placeholder('yyyy-mm-dd')
                                    }}
                            <span class="text-danger">{{ $errors->first('birth_date') }}</span>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary" type="submit">Edit User</button>
                            <a href="{{ route('users.index') }}" class="btn btn-default" type="reset">Cancel</a>
                        </div>
                    {{ html()->form()->close() }}
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-js')
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>

    <script>
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: false
        });
    </script>
@stop