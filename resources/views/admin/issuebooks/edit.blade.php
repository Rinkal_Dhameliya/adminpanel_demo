@extends('admin.layout.index')

@section('title')
    Edit Issue Book
@stop

@section('page-css')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">
@stop


@section('content')
    <div class="page-title">
        <h3>Edit Issue Book</h3>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="grid simple">
                <div class="grid-body no-border">
                    
                {{ html()->form('PUT', route('issuebooks.update', $issuebook->getKey()))->open() }}
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label">User Name</label>
                            <div class="droplist">
                                {{
                                html()
                                    ->select('user',$users, old('user', $issuebook->user))
                                    ->class('form-control')
                            }}
                                <span class="error">{{ $errors->first('user') }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label">Book Name</label>
                            <div class="droplist">
                                {{
                                html()
                                    ->select('book',$books, old('book', $issuebook->book)  )
                                    ->class('form-control')
                            }}
                                <span class="error">{{ $errors->first('book') }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label">Issue Date</label>
                            <div class="input-with-icon right">
                                {{
                                    html()
                                    ->input('text', 'issue_date', old('issue_date', $issuebook->issue_date))
                                    ->class('form-control datepicker')
                                    ->placeholder('yyyy-mm-dd')
                                    }}

                                <span class="text-danger">{{ $errors->first('issue_date') }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label">Expiry Date</label>
                            <div class="input-with-icon right">
                                {{
                                    html()
                                    ->input('text', 'expiry_date', old('expiry_date', $issuebook->expiry_date))
                                    ->class('form-control datepicker')
                                    ->placeholder('yyyy-mm-dd')
                                    }}

                                <span class="text-danger">{{ $errors->first('expiry_date') }}</span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <button class="btn btn-primary" type="submit">Edit Issue Book</button>
                            <a href="{{route('issuebooks.index')}}" class="btn btn-white">Cancel</a></div>
                    </div>
                </div>
                {{ html()->form()->close() }}
            </div>
        </div>
    </div>
</div>
@stop

@section('page-js')
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>

   
    <script>
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: false
        });
    </script>
@stop
