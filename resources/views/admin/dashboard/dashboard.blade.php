@extends('admin.layout.index')

@section('title')
  Admin Dashboard
@stop

@section('page-css')
  <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
@stop

@section('content')

<!-- main content start-->
<div class="col_3">
  <div class="col-md-3 widget widget1">
    <div class="r3_counter_box">
      <i class="pull-left fa fa-user icon-rounded"></i>
      <div class="stats">
        <h5><strong>Total User</strong></h5>
        <span data-value="{{ $totalUsers }}">{{ $totalUsers }}</span>
      </div>
    </div>
  </div>
  <div class="col-md-3 widget widget1">
    <div class="r3_counter_box">
      <i class="pull-left fa fa-book user1 icon-rounded"></i>
      <div class="stats">
        <h5><strong>Total Book</strong></h5>
        <span data-value="{{ $totalBooks }}">{{ $totalBooks }}</span>
      </div>
    </div>
  </div>
  <div class="col-md-3 widget widget1">
    <div class="r3_counter_box issue-book">
      <i class="pull-left fa fa-book dollar1 icon-rounded"></i>
      <div class="stats">
        <h5><strong>Total Issue Book</strong></h5>
        <span data-value="{{ $totalIssueBooks }}">{{ $totalIssueBooks }}</span>
      </div>
    </div>
  </div>
</div>

<div class="clear" style="padding-top:13%"></div>

 <div class="col-md-6 pull-left">
    <div class="grid simple">
        <div class="grid-body no-border">
          <div class="pull-left head-list">List Of Today's Renew Book</div>
          <div class="pull-right head-date">{{ $format}}</div>
            {!! $htmldata->table() !!}
        </div>
    </div>
</div>

<div class="row">
  <div class="col-md-6 pull-right">
    <div class="panel panel-default">
      <div class="panel-heading">Today Return Book <span class="pull-right">{{ $format}}</span></div>
        <div class="panel-body">
          <table class="table table-hover table-bordered table-striped datatable" style="width:100%">
            <thead>
              <tr>
                  <th>Id</th>
                  <th>User Name</th> 
                  <th>Book Name</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
  </div>
</div>


@stop

@section('page-js')

<script type="text/javascript">
function changeBooks(id){
  $.ajax({
    url: '{{ route('dashboard-request:update:book') }}',
    type: 'POST',
    data: {
            _token: '{{ csrf_token() }}',
            id: id
        },
    cache: false,
    beforeSend: function () {
      $(this).attr('disabled', 'disabled');
    },
    success: function (result) {
       console.log(result);
       debugger;
        $(this).removeAttr('disabled', 'disabled');
        if (result.success) {
            window.location.reload(true);
        } else {
           alert('something went wrong..!!');
        }
    }
  });
}   
</script>


<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    {!! $htmldata->scripts() !!}


<script type="text/javascript">
  $(document).ready(function() {
    $('.datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ route('dashboard-second:postdata') }}',
        type: "GET",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'user.name', name: 'user_id'},
            {data: 'book.book_name', name: 'book_id'},
        ]
    });
});
</script>


@stop
