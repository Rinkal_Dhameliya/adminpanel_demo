@extends('admin.layout.index')

@section('title')
    Issue Book Request List
@stop

@section('page-css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
@stop

@section('content')

<div class="page-title">
    <h3>Manage Issue-Book Request</h3>

 	<!-- <a href="{{ route('issuebooks.create') }}" class="btn btn-primary pull-right">Add Issue Book</a> -->
</div>

<div class="row">
        <div class="col-md-12">
            <div class="grid simple">
                <div class="grid-body no-border">
                    {!! $html->table() !!}
                </div>
            </div>
        </div>
    </div>
@stop


@section('page-js')

<script>
    function changeStatus(id){
      
        if(confirm("Are you sure you want to approve this user request ?")){
            $.ajax({
            url: '{{ route('issuebook-request:update:status') }}',
            type: 'POST',
            data: {
                    _token: '{{ csrf_token() }}',
                    issuebook_id: id
                },
            cache: false,
            beforeSend: function () {
                $(this).attr('disabled', 'disabled');
            },
            success: function (result) {
               console.log(result);
               debugger;
                $(this).removeAttr('disabled', 'disabled');
                if (result.success) {
                    window.location.reload(true);
                } else {
                   alert('something went wrong..!!');
                }
            }
        });
    }   
}
</script>

<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    {!! $html->scripts() !!}
@stop