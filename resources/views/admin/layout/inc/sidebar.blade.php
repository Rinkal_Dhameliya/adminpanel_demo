<aside class="sidebar-left">
  <nav class="navbar navbar-inverse">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <div class="navbar-brand"><h1>Admin</h1></div>
      </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="sidebar-menu">
          <li class="treeview {{ request()->is('admin/dashboard*') ? 'active' : '' }}">
            <a href="{{ route('admin:dashboard') }}">
            <i class="fa fa-home"></i> <span>Dashboard</span>
            </a>
          </li>
          <li class="treeview {{ request()->is('admin/users*') ? 'active' : '' }}">
            <a href="{{ url('admin/users') }}">
            <i class="fa fa-user"></i>
            <span>User</span>
            </a>
          </li>
          <li class="treeview {{ request()->is('admin/categorys*') ? 'active' : '' }}">
            <a href="{{ url('admin/categorys') }}">
            <i class="fa fa-user"></i>
            <span>Category</span>
            </a>
          </li>
          <li class="treeview {{ request()->is('admin/books*') ? 'active' : '' }}">
            <a href="{{ url('admin/books') }}">
            <i class="fa fa-book"></i>
            <span>Book</span>
            </a>
          </li>
           <li class="treeview {{ request()->is('admin/issuebooks*') ? 'active' : '' }}">
            <a href="{{ url('admin/issuebooks') }}">
            <i class="fa fa-book"></i>
            <span>Issue Book</span>
            </a>
          </li>
          <li class="treeview {{ request()->is('admin/issuebook-request*') ? 'active' : '' }}">
            <a href="{{ url('admin/issuebook-request') }}">
            <i class="fa fa-book"></i>
            <span>Issue Book Request</span>
            </a>
          </li> 
        </ul>
      </div>
  </nav>
</aside>