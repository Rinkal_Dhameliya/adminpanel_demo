<!DOCTYPE HTML>
<html>
<head>
<title>@yield('title') | Projects</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href="{{ asset('/') }}admin/css/bootstrap.css" rel='stylesheet' type='text/css' />

<link href="{{ asset('/') }}admin/css/style.css" rel='stylesheet' type='text/css' />

<link href="{{ asset('/') }}admin/css/font-awesome.css" rel="stylesheet"> 

<link href="{{ asset('/') }}admin/css/SidebarNav.min.css" media='all' rel='stylesheet' type='text/css'/>


<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- Metis Menu -->
<script src="{{ asset('/') }}admin/js/metisMenu.min.js"></script>
<link href="{{ asset('/') }}admin/css/custom.css" rel="stylesheet">
<!--//Metis Menu -->

<link href="{{ asset('/') }}admin/js/sweetalert2/sweetalert2.css" rel="stylesheet" type="text/css">
@yield('page-css')
</head> 

<body class="cbp-spmenu-push">
  <div class="main-content">
    <div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
      <!--left-fixed -navigation-->
      @include('admin.layout.inc.sidebar')
    </div>
      <!--left-fixed -navigation-->
    
    <!-- header-starts -->
    <div class="sticky-header header-section ">
      <div class="header-right">
        <div class="profile_details">   
          <ul>
            <li class="dropdown profile_details_drop">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <div class="profile_img"> 
                  <span class="prfil-img"><img src="/admin/images/default-user.png"alt="" height="40"> </span>  
                  <div class="user-name" id="drplist">
                    <p>{{ auth('admin')->user()->username}}</p>
                    <i class="fa fa-angle-down lnr"></i>
                    <i class="fa fa-angle-up lnr"></i>
                  </div>
                  <div class="clearfix"></div>  
                </div>  
              </a>
              <ul class="dropdown-menu drp-mnu" id="drpmenu">
                <li> <a href="{{ route('profile.index') }}"><i class="fa fa-user"></i> Profile Settings</a> </li> 
                <li> <a href="{{ route('admin:change:password') }}"><i class="fa fa-suitcase"></i> Change Password</a> </li> 
                <li> <a href="{{ route('admin:logout') }}"><i class="fa fa-sign-out"></i> Log Out</a> </li>
              </ul>
            </li>
          </ul>
        </div>
        <div class="clearfix"> </div>       
      </div>
      <div class="clearfix"> </div> 
    </div>
    <!-- //header-ends -->

    <!-- main content start-->
    <div id="page-wrapper">
      <div class="main-page">
           @yield('content')
      </div>
    </div>
  </div>
   
<!--scrolling js-->
<script src="{{ asset('/') }}admin/js/jquery.nicescroll.js"></script>
<script src="{{ asset('/') }}admin/js/scripts.js"></script>
<!--//scrolling js-->

<script src="{{ asset('/') }}admin/js/jquery-1.11.1.min.js"></script>
<script src="{{ asset('/') }}admin/js/modernizr.custom.js"></script>



<!-- Bootstrap Core JavaScript -->
 <script src="{{ asset('/') }}admin/js/bootstrap.js"> </script>
<!-- //Bootstrap Core JavaScript -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script src="{{ asset('/') }}admin/js/bootstrap-notify-master/bootstrap-notify.min.js"></script>
<script src="{{ asset('/') }}admin/js/sweetalert2/sweetalert2.js"></script>
<script>
    @if(session()->has('success'))
        getSweetAlertSuccessMessage('{{ session()->get('success') }}');
    @elseif(session()->has('error'))
        getSweetAlertErrorMessage('{{ session()->get('error') }}');
    @elseif(session('status'))
    $.notify({
        icon: 'fa fa-thumbs-o-up',
        message: "{{ session('status') }}"
    },{
        animate: {
            enter: 'animated flipInY',
            exit: 'animated flipOutX'
        },
        type: 'success'
    });
    @endif

    function getSweetAlertErrorMessage(message)
    {
        swal(
                'Oops...',
                message,
                'error'
        );
    }

    function getSweetAlertSuccessMessage(message)
    {
        swal(
                'Success',
                message,
                'success'
        );
    }
</script> 

@yield('page-js')
</body>
</html>