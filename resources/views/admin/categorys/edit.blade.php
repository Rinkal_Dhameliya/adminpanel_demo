@extends('admin.layout.index')

@section('title')
    Edit Category
@stop

@section('page-css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
@stop

@section('content')

    <div class="page-title">
        <h3>Edit Category</h3>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="grid simple">
                <div class="grid-body no-border">
                    @include('admin.shared.messages')

                    {{ html()->form('POST', route('categorys.update', $category->getKey()))->open() }}
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="form-group">
                            <label for="">Name</label>
                            {{
                                html()
                                    ->input('text', 'category_name', old('category_name', $category->category_name))
                                    ->class('form-control')
                                    ->placeholder('Category Name')
                            }}
                            <span class="error">{{ $errors->first('category_name') }}</span>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary" type="submit">Edit Category</button>
                            <a href="{{ route('categorys.index') }}" class="btn btn-default" type="reset">Cancel</a>
                        </div>
                    {{ html()->form()->close() }}
                </div>
            </div>
        </div>
    </div>
@stop