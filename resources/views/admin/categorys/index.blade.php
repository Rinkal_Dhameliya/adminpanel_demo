@extends('admin.layout.index')

@section('title')
    Category List
@stop

@section('page-css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
 
@stop

@section('content')

<div class="page-title">
    <h3>Manage Category</h3>
</div>

<div class="container" id="modelcontent" style="padding-right: 30%;">
   
  <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal" id="add_data">Add Category</button>

  <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Category</h4> 
                </div>
                <div class="modal-body">
                    <div class="tiers">
                        <div class="grid-body no-border">
                            <div class="row">
                                <div class="col-xs-12 add-tier-form">
                                    {{ html()->form('POST')->id('cat_add')->class('add-shifts-form')->acceptsFiles()->open() }}
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="">Category Name</label>
                                        {{
                                            html()
                                            ->input('text', 'category_name', old('category_name'))
                                            ->class('form-control')->id('category_name')->attribute('required')
                                            ->placeholder('Category Name')
                                        }}
                                        <span class="error">{{ $errors->first('category_name') }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                       <span class="edit-cancel-button-append"></span>
                        <input type="submit" name="submit" id="action" value="Add Category" class="btn btn-info add-tier-btn enableOnInput">
                        <button type="button" class="btn btn-default custom-btn" data-dismiss="modal">close</button>
                    </div>
                    {{ html()->form()->close() }}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="grid simple">
            <div class="grid-body no-border">
                {!! $html->table() !!}
            </div>
        </div>
    </div>
</div>
@stop

 @section('page-js')

<script type="text/javascript">

    $(document).ready(function(){
        $("#action").click(function(e){
            e.preventDefault();
            var category_name = $("#category_name").val();
            $.ajax({
                type: "POST",
                url: "{{ route('categorys.store') }}",             
                data: {category_name: category_name, _token: '{{csrf_token()}}' },
                success: function(response){ 
                    //$("#myModal").modal('hide');
                    //$(".modal-backdrop.in").hide();
                    swal("Success", "Category added successfully!", "success");
                   location.reload(true);
                }
            });
            return false;
        });
    });
</script>
  
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    {!! $html->scripts() !!}
@stop