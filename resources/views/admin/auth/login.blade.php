@extends('admin.auth.layout')

@section('title')
    Admin Login | Project
@stop

@section('content')
		<div class="main-page login-page">
			<h2 class="title1">Admin Login</h2>
			<div class="widget-shadow">
				<div class="login-body">
					<form action="{{ route('post:admin:login') }}" method="post">
						{{ csrf_field() }}

						@if(\Session::has('error'))
	                		<div class="row">
	                    		<div class="alert alert-danger col-md-12">{{ \Session::get('error') }}</div>
	                		</div>
            			@endif
			            @if(\Session::has('success'))
			                <div class="row">
			                    <div class="alert alert-success col-md-12">{{ \Session::get('success') }}</div>
			                </div>
			            @endif

			            <div class="input-with-icon right">
							<input type="email" class="user" name="email" placeholder="Enter Your Email">
							<span class="text-danger">{{ $errors->first('email') }}</span>
						</div>	

						<div class="input-with-icon right">	
							<input type="password" name="password" class="lock" placeholder="Password">
							<span class="text-danger">{{ $errors->first('password') }}</span>
						</div>

						<div class="forgot-grid">
							<div class="forgot">
								<a href="{{ route('admin:forgot:password') }}">forgot password?</a>
							</div>
							<div class="clearfix"> </div>
						</div>
						<input type="submit" name="Sign In" value="Sign In">	
					</form>
				</div>
			</div>	
		</div>
@stop