@extends('admin.auth.layout')

@section('title')
    Forgot Password | Project
@stop

@section('content')

	<div class="main-page login-page ">
			<h2 class="title1">Forgot Password</h2>
			<div class="widget-shadow">
				<div class="login-body">
					<form action="{{ route('post:admin:forgot:password') }}" method="post">
						{{ csrf_field() }}

						@if(\Session::has('error'))
		                <div class="row">
		                    <div class="alert alert-danger col-md-12">{{ \Session::get('error') }}</div>
		                </div>
		            	@endif

			            <div class="input-with-icon right">
							<input type="email" class="user" name="email" placeholder="Enter Your Email">
							<span class="text-danger">{{ $errors->first('email') }}</span>
						</div>	
						
						<div class="row">
			                <div class="col-md-12">
			                    <button class="btn btn-primary btn-cons btn-block" type="submit">Send Email</button>
			                </div>
           				</div>	
					</form>
				</div>
			</div>	
		</div>

@stop

