<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Verified Account | Admin Project</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link rel="shortcut icon" type="image/png" href="{{ asset('admin/img/icon/favicon-32x32.png') }}"/>

    <!-- BEGIN CORE CSS FRAMEWORK -->
    <link href="{{ asset('/') }}/admin/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="{{ asset('/') }}/admin/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/') }}/admin/plugins/boostrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/') }}/admin/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/') }}/admin/css/animate.min.css" rel="stylesheet" type="text/css"/>
    <!-- END CORE CSS FRAMEWORK -->
    <!-- BEGIN CSS TEMPLATE -->
    <link href="{{ asset('/') }}/admin/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/') }}/admin/css/responsive.css" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/') }}/admin/css/custom-icon-set.css" rel="stylesheet" type="text/css"/>
    <!-- END CSS TEMPLATE -->
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="error-body no-top">
<div class="container">
    <div class="row login-container column-seperation">
        <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
           
        </div>
    </div>
    <h1 class="text-center" style="font-weight: 600;font-size: 35px;margin-top: 30px">
        Your account has been verified.
    </h1>
</div>
<!-- END CONTAINER -->
<!-- BEGIN CORE JS FRAMEWORK-->
<script src="{{ asset('/') }}/admin/plugins/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="{{ asset('/') }}/admin/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{ asset('/') }}/admin/plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="{{ asset('/') }}/admin/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="{{ asset('/') }}/admin/js/login.js" type="text/javascript"></script>
<!-- BEGIN CORE TEMPLATE JS -->
<!-- END CORE TEMPLATE JS -->
</body>
</html>