<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Book;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_id');
            $table->foreign('category_id')->references('id')->on('categorys')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->string('book_name');
            $table->string('author_name');
            $table->float('price');
            $table->timestamps();
        });
        Book::create([
            'category_id' => 1,
            'book_name' => 'Five Point Someone',
            'author_name' => 'Chetan Bhagat',
            'price' => 200
        ]);
        Book::create([
            'category_id' => 2,
            'book_name' => 'Book Title',
            'author_name' => 'Chetan Bhagat',
            'price' => 250
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
