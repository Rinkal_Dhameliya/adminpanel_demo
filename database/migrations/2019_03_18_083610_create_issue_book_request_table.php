<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\RequestIssueBook;

class CreateIssueBookRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('issue_book_request', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('issuebook_id');
            $table->foreign('issuebook_id')->references('id')->on('issue_books')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->enum('status', ['pending', 'approved'])->default('pending');
        });
        RequestIssueBook::create([
            'issuebook_id' => 1,
            'status' => 'pending'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('issue_book_request');
    }
}
