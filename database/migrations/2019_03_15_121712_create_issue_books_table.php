<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\IssueBook;

class CreateIssueBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('issue_books', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->unsignedInteger('book_id');
            $table->foreign('book_id')->references('id')->on('books')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->dateTime('issue_date');
            $table->dateTime('expiry_date');
           // $table->boolean('retn_book')->default(false);
            $table->enum('retn_book', ['pending', 'returned'])->default('pending');
            $table->enum('status', ['pending', 'approved'])->default('pending');
            $table->timestamps();
        });
        IssueBook::create([
            'user_id' => 1,
            'book_id'=> 1,
            'issue_date' => '2019-03-12',
            'expiry_date' => '2019-05-20'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('issue_books');
    }
}
