<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\User;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->dateTime('birth_date')->nullable();
            $table->string('email');
            $table->string('password')->nullable();
             $table->string('google_id')->nullable();
            $table->boolean('is_active')->default(true);
            $table->string('verification_code')->nullable();
            $table->boolean('is_verified')->default(false);
            $table->string('user_password_token')->nullable();
            $table->string('forgot_password_token')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
        User::create([
            'name' => 'user',
            'email' => 'user@gmail.com',
            'password' => bcrypt('123456'),
            'birth_date' => '1997-03-12',
            'is_verified' => true
        ]);
        User::create([
            'name' => 'xyz',
            'email' => 'xyz@xyz.com',
            'password' => bcrypt('123456'),
            'birth_date' => '1997-05-20',
            'is_verified' => true
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
