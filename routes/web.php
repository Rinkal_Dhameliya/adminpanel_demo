<?php

/*Route::redirect('/', 'user/login');
*/
Route::get('/', function () {
    return view('welcome');
});


Route::get('verify-account/{code}', 'User\Auth\RegisterController@verifyAccount')->name('verify:account');

// User
Route::group(['prefix' => 'user',
    'namespace' => 'User'], function () {
    Route::group(['middleware' => ['guest:web']], function () {
    
    //login
    Route::get('login', ['as' => 'user:login', 'uses' => 'Auth\LoginController@getLogin']);
    Route::post('login', ['as' => 'post:user:login', 'uses' => 'Auth\LoginController@postLogin']);

    Route::get('redirect', ['as' => 'user:login:redirect', 'uses' => 'Auth\SocialAuthGoogleController@redirect']);

    Route::get('callback', ['as' => 'user:login:call', 'uses' => 'Auth\SocialAuthGoogleController@callback']);


    //sigup
    Route::get('register', ['as' => 'user:register', 'uses' => 'Auth\RegisterController@getRegister']);
    Route::post('register', ['as' => 'post:user:register', 'uses' => 'Auth\RegisterController@postRegister']);

     // Password Reset Routes...
        Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('user:forgot:password');
        Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('post:user:forgot:password');
        Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('user:reset:password');
        Route::post('password/reset/{id}', 'Auth\ResetPasswordController@reset')->name('post:user:reset:password');
    });

    Route::group(['middleware' => 'auth:web'], function () {

        Route::get('logout', ['as' => 'user:logout', 'uses' => 'Auth\LoginController@getLogout']);


        // user dashboard
        Route::get('dashboard', ['as' => 'user:dashboard', 'uses' => 'DashboardController@index']);

        Route::get('dashboard-user-second', 'DashboardController@getPosts')->name('dashboard-user-second:postdata');

        // user profile
        Route::resource('userprofile', 'ProfileController', ['only' => ['index', 'store']]);

        // user change password
        Route::get('user-change-password', ['as' => 'user:change:password', 'uses' => 'SettingsController@userChangePassword']);
        Route::post('user-change-password', ['as' => 'post:user:change:password', 'uses' => 'SettingsController@postUserChangePassword']);

        //book
        Route::get('book', ['as' => 'user:book', 'uses' => 'BookController@index']);

        Route::resource('issue-books', 'IssueBookController');

        //Route::get('stripe', 'StripePaymentController@stripe');
        
        Route::post('stripe', 'StripePaymentController@stripePost')->name('stripe.post');
    });
});



// Admin

Route::group(['prefix' => 'admin',
    'namespace' => 'Admin'], function () {
    Route::group(['middleware' => ['guest:admin']], function () {
        Route::get('login', ['as' => 'admin:login', 'uses' => 'LoginController@getLogin']);
        Route::post('login', ['as' => 'post:admin:login', 'uses' => 'LoginController@postLogin']);

        // Password Reset Routes...
        Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('admin:forgot:password');
        Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('post:admin:forgot:password');
        Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('admin:reset:password');
        Route::post('password/reset/{id}', 'ResetPasswordController@reset')->name('post:admin:reset:password');

	});

  Route::get('password/user/{token}', 'UserPasswordController@showResetForm')->name('admin:user:password');
        Route::post('password/reset/{id}', 'UserPasswordController@reset')->name('post:admin:user:password');


	Route::group([
        'middleware' => 'auth:admin'
    ], function () {
        Route::get('logout', ['as' => 'admin:logout', 'uses' => 'LoginController@getLogout']);

        Route::get('dashboard', ['as' => 'admin:dashboard', 'uses' => 'DashboardController@index']);

         Route::get('dashboard-second', 'DashboardController@getPosts')->name('dashboard-second:postdata');


        Route::post('dashboard-request/update/book', 'DashboardController@updateBook')->name('dashboard-request:update:book');

        Route::resource('users', 'UserController');

        Route::resource('categorys', 'CategoryController');

        Route::resource('books', 'BookController');

        Route::resource('issuebooks', 'IssueBookController');

        Route::resource('issuebook-request', 'IssueBookRequestController');

        Route::post('issuebook-request/update/status', 'IssueBookRequestController@updateStatus')->name('issuebook-request:update:status');

        Route::resource('profile', 'ProfileController', ['only' => ['index', 'store']]);

        Route::get('change-password', ['as' => 'admin:change:password', 'uses' => 'SettingsController@adminChangePassword']);
        Route::post('change-password', ['as' => 'post:admin:change:password', 'uses' => 'SettingsController@postAdminChangePassword']);
     });
});