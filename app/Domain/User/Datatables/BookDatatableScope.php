<?php

namespace App\Domain\User\Datatables;

use App\Domain\Util\Datatables\NotActionDatatableScope;
use Carbon\Carbon;
use App\Models\Book;

class BookDatatableScope extends NotActionDatatableScope
{
    /**
     * AppDatatableScope constructor.
     */
    public function __construct()
    {
        $this->setHtml([
            [
                'data' => 'book_name',
                'name' => 'book_name',
                'title' => 'Book Name',
            ],
            [
                'data' => 'author_name',
                'name' => 'author_name',
                'title' => 'Author Name',
            ],
            [
                'data' => 'category',
                'name' => 'category_id',
                'title' => 'Category',
            ],
            [
                'data' => 'price',
                'name' => 'price',
                'title' => 'Price',
            ],
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function query()
    {
        $query = Book::query()->with(['category']);

        return datatables()->eloquent($query)
        ->editColumn('category', function ($model) {
                return $model->category->category_name;
            })
            ->make(true);
    }
}