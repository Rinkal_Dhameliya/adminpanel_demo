<?php

namespace App\Domain\User\Datatables;

use App\Domain\Util\Datatables\NotActionDatatableScope;
use Carbon\Carbon;
use App\Models\Book;
use App\Models\IssueBook;

class DashboardDatatableScope extends NotActionDatatableScope
{
    /**
     * AppDatatableScope constructor.
     */
    public function __construct()
    {
        $this->setHtml([
            [
                'data' => 'user',
                'name' => 'user_id',
                'title' => 'User Name',
            ],
            [
                'data' => 'book',
                'name' => 'book_id',
                'title' => 'Book Name',
            ],
            
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function query()
    {
       //$events= IssueBook::whereDate('expiry_date','=',Carbon::now());

       $events = auth()->user()->isbook()->with(['book','user'])->whereDate('expiry_date','=',Carbon::now())->where('retn_book', '=', 'pending');

       return datatables()->eloquent($events)
        ->editColumn('book', function ($model) {
                return $model->book->book_name;
            })
        ->editColumn('user', function ($model) {
                return $model->user->name;
            })
           
            ->make(true);
    }
}