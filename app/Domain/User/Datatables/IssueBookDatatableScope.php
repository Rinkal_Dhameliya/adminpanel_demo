<?php

namespace App\Domain\User\Datatables;

use App\Domain\Util\Datatables\NotActionDatatableScope;
use Carbon\Carbon;
use App\Models\IssueBook;
use App\Models\RequestIssueBook;

class IssueBookDatatableScope extends NotActionDatatableScope
{
    /**
     * AppDatatableScope constructor.
     */
    public function __construct()
    {
        $this->setHtml([
            [
                'data' => 'user',
                'name' => 'user_id',
                'title' => 'User Name',
            ],
            [
                'data' => 'book',
                'name' => 'book_id',
                'title' => 'Book Name',
            ],
            [
                'data' => 'issue_date',
                'name' => 'issue_date',
                'title' => 'Issue Date',
            ],
            [
                'data' => 'expiry_date',
                'name' => 'expiry_date',
                'title' => 'Expiry Date',
            ],
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function query()
    {
        //$query = IssueBook::query()->with(['book','user']);

        $query = auth()->user()->isbook()->with(['book','user'])->where('retn_book', '=', 'pending');

        //$query = RequestIssueBook::query()->where('status', '=', 'approved')->with(['bookreturn']);

        return datatables()->eloquent($query)
        ->editColumn('book', function ($model) {
                return $model->book->book_name;
            })
        ->editColumn('user', function ($model) {
                return $model->user->name;
            })
        ->editColumn('issue_date', function ($model) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $model->issue_date)->format('Y-m-d');
            })
        ->editColumn('expiry_date', function ($model) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $model->expiry_date)->format('Y-m-d');
            })
           
            ->make(true);
    }
}