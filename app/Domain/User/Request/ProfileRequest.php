<?php

namespace App\Domain\User\Request;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = auth('web')->id();
        return [
            'email' => 'required|unique:users,email,' . $id,
            'name' => 'required|unique:users,name,' . $id,
            'birth_date' => 'required|date_format:Y-m-d'
        ];
    }

    public function persist()
    {
        return [
            'email' => $this->get('email'),
            'name' => $this->get('name'),
            'birth_date' => $this->get('birth_date')
        ];
    }
}