<?php

namespace App\Domain\User\Request;

use Illuminate\Foundation\Http\FormRequest;

class IssueBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           /* 'status' => 'required',
            'issuebook_id' => 'required|exists:issue_books,id',*/
            'issue_date' => 'required|date_format:Y-m-d',
            'expiry_date' => 'required|date_format:Y-m-d',
            'book' => 'required|exists:books,id',
            'user_id' => 'required|exists:users,id'
        ];
    }

    public function persist()
    {   
        return array_merge(
            /*['status' => $this->get('status')],
            ['issuebook_id' => $this->get('bookreturn')],*/

            ['issue_date' => $this->get('issue_date')],
            ['expiry_date' => $this->get('expiry_date')],
            ['book_id' => $this->get('book')],
            ['user_id' => $this->get('user_id')]
        );
    }
}