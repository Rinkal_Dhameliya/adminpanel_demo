<?php

namespace App\Domain\Admin\Request;

use Illuminate\Foundation\Http\FormRequest;

class IssueBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'issue_date' => 'required|date_format:Y-m-d',
            'expiry_date' => 'required|date_format:Y-m-d',
            'book' => 'required|exists:books,id',
            'user' => 'required|exists:users,id'
        ];
    }

    public function persist()
    {   
        return array_merge(
            ['issue_date' => $this->get('issue_date')],
            ['expiry_date' => $this->get('expiry_date')],
            ['book_id' => $this->get('book')],
            ['user_id' => $this->get('user')]
        );
    }
}