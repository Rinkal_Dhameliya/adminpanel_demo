<?php

namespace App\Domain\Admin\Request;

use Illuminate\Foundation\Http\FormRequest;

class BookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'book_name' => 'required',
            'author_name' => 'required',
            'category' => 'required|exists:categorys,id',
            'price' => 'required'
        ];
    }

    public function persist()
    {   
        return array_merge(
            $this->only('book_name', 'author_name','price'),
            ['category_id' => $this->get('category')]
        );
    }
}
