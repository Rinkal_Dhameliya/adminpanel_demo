<?php

namespace App\Domain\Admin\Request;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = auth('admin')->id();
        return [
            'email' => 'required|unique:admin_users,email,' . $id,
            'name' => 'required|unique:admin_users,username,' . $id
        ];
    }

    public function persist()
    {
        return [
            'email' => $this->get('email'),
            'username' => $this->get('name')
        ];
    }
}