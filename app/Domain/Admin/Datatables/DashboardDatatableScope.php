<?php

namespace App\Domain\Admin\Datatables;

use App\Domain\Util\Datatables\NotActionDatatableScope;
use Carbon\Carbon;
use App\Models\Book;
use App\Models\IssueBook;

class DashboardDatatableScope extends NotActionDatatableScope
{
    /**
     * AppDatatableScope constructor.
     */
    public function __construct()
    {
        $this->setHtml([
            [
                'data' => 'user',
                'name' => 'user_id',
                'title' => 'User Name',
            ],
            [
                'data' => 'book',
                'name' => 'book_id',
                'title' => 'Book Name',
            ],
            [
                'data' => 'retn_book',
                'name' => 'retn_book',
                'title' => 'Add',
            ]
           
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function query()
    {
       $query= IssueBook::whereDate('expiry_date','=',Carbon::now())->where('retn_book', '=', 'pending');

       // $bookquery = RequestIssueBook::query()->where('status', '=', 'approved');

       return datatables()->eloquent($query)
        ->editColumn('book', function ($model) {
                return $model->book->book_name;
            })
        ->editColumn('user', function ($model) {
                return $model->user->name;
            })

        ->editColumn('retn_book', function ($model) {
                if($model->retn_book === "pending"){
                return '<a href="javascript:window.onclick=changeBooks('.$model->id.')" class="'.$model->retn_book.'">'.ucwords($model->retn_book).'</a>';
                }else{
                    return '<a class="'.$model->retn_book.'">'.ucwords($model->retn_book).'</a>';
                }
            })
        
            ->rawColumns(['retn_book'])
            ->make(true);
    }
}