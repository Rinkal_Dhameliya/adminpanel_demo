<?php

namespace App\Domain\Admin\Datatables;

use App\Domain\Util\Datatables\BaseDatatableScope;
use Carbon\Carbon;
use App\Models\IssueBook;
use App\Models\RequestIssueBook;

class IssueBookDatatableScope extends BaseDatatableScope
{
    /**
     * AppDatatableScope constructor.
     */
    public function __construct()
    {
        $this->setHtml([
            [
                'data' => 'user',
                'name' => 'user_id',
                'title' => 'User Name',
            ],
            [
                'data' => 'book',
                'name' => 'book_id',
                'title' => 'Book Name',
            ],
            [
                'data' => 'issue_date',
                'name' => 'issue_date',
                'title' => 'Issue Date',
            ],
            [
                'data' => 'expiry_date',
                'name' => 'expiry_date',
                'title' => 'Expiry Date',
            ],
          
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function query()
    {
       /*$query = IssueBook::query()->with(['book','user']);*/

        //auth()->user()->isbook()->with(['book','user'])->where('retn_book', '=', 'pending'));

        $query = RequestIssueBook::query()->where('status', '=', 'approved');

        return datatables()->eloquent($query)
        ->editColumn('book', function ($model) {
                return $model->bookreturn->book->book_name;
            })
        ->editColumn('user', function ($model) {
                return $model->bookreturn->user->name;
            })
        ->editColumn('issue_date', function ($model) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $model->bookreturn->issue_date)->format('Y-m-d');
            })
        ->editColumn('expiry_date', function ($model) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $model->bookreturn->expiry_date)->format('Y-m-d');
            })
        
            ->addColumn('actions', function ($model) {   

                return view('admin.shared.dtcontrols')
                    ->with('id', $model->getKey())
                    ->with('model', $model)
                    ->with('editUrl', route('issuebooks.edit', $model->getKey()))
                    ->with('deleteUrl', route('issuebooks.destroy', $model->getKey()))
                    ->render();
            })
           
            ->rawColumns(['actions'])
            ->make(true);
    }
}