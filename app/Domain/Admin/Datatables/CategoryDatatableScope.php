<?php

namespace App\Domain\Admin\Datatables;

use App\Domain\Util\Datatables\BaseDatatableScope;
use App\Models\Category;

class CategoryDatatableScope extends BaseDatatableScope
{
    /**
     * AppDatatableScope constructor.
     */
    public function __construct()
    {
        $this->setHtml([
            [
                'data' => 'category_name',
                'name' => 'category_name',
                'title' => 'Category Name',
            ],
            [
                'data' => 'total_book',
                'name' => 'total_book',
                'title' => 'Total book',
            ],
            [
                'data' => 'is_active',
                'name' => 'is_active',
                'title' => 'Is Active?',
                'searchable' => false,
                'orderable' => false
            ],
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function query()
    {
        $categorys = Category::with('books')->get();

        return datatables()->eloquent(Category::query())
        ->addColumn('total_book', function ($model) {
                return $model->books()->count();
            })

        ->editColumn('is_active', function ($model) {
                return $model->is_active ? 'YES' : 'NO';
        })
            ->addColumn('actions', function ($model) {
               
                return view('admin.shared.dtcontrols')
                    ->with('id', $model->getKey())
                    ->with('model', $model)
                    ->with('editUrl', route('categorys.edit', $model->getKey()))
                    ->with('deleteUrl', route('categorys.destroy', $model->getKey()))
                    ->render();
            })
           
            ->rawColumns(['actions','total_book'])
            ->make(true);
    }
}