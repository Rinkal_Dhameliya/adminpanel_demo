<?php

namespace App\Domain\Admin\Datatables;

use App\Domain\Util\Datatables\BaseDatatableScope;
use Carbon\Carbon;
use App\Models\IssueBook;
use App\Models\RequestIssueBook;

class IssueBookRequestDatatableScope extends BaseDatatableScope
{
    /**
     * AppDatatableScope constructor.
     */
    public function __construct()
    {
        $this->setHtml([
            [
                'data' => 'username',
                'name' => 'issuebook_id',
                'title' => 'User Name',
            ],
            [
                'data' => 'bookname',
                'name' => 'issuebook_id',
                'title' => 'Book Name',
            ],
            [
                'data' => 'issuedate',
                'name' => 'issuebook_id',
                'title' => 'Issue Date',
            ],
            [
                'data' => 'expirydate',
                'name' => 'issuebook_id',
                'title' => 'Expiry Date',
            ],
            [
                'data' => 'status',
                'name' => 'status',
                'title' => 'Status',
            ],
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function query()
    {
        $query = RequestIssueBook::query()->with(['bookreturn']);
        return datatables()->eloquent($query)
        
        ->editColumn('username', function ($model) {
                return $model->bookreturn->user->name;
            })
        ->editColumn('bookname', function ($model) {
                return $model->bookreturn->book->book_name;
            })
         ->editColumn('issuedate', function ($model) {
            return Carbon::createFromFormat('Y-m-d H:i:s', $model->bookreturn->issue_date)->format('Y-m-d');
            })
         ->editColumn('expirydate', function ($model) {
            return Carbon::createFromFormat('Y-m-d H:i:s', $model->bookreturn->expiry_date)->format('Y-m-d');
            })
        
           ->editColumn('status', function ($model) {
                if($model->status === "pending"){
                return '<a href="javascript:window.onclick=changeStatus('.$model->id.')" class="'.$model->status.'">'.ucwords($model->status).'</a>';
                }else{
                    return '<a class="'.$model->status.'">'.ucwords($model->status).'</a>';
               
                }
            })

            ->addColumn('actions', function ($model) {   

                return view('admin.shared.dtcontrols')
                    ->with('id', $model->getKey())
                    ->with('model', $model)
                    ->with('editUrl', route('issuebook-request.edit', $model->getKey()))
                    ->with('deleteUrl', route('issuebook-request.destroy', $model->getKey()))
                    ->render();
            })
           
            ->rawColumns(['actions','status'])
            ->make(true);
    }
}