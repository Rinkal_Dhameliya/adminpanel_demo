<?php

namespace App\Domain\Admin\Datatables;

use App\Domain\Util\Datatables\BaseDatatableScope;
use App\Models\User;
use Carbon\Carbon;

class UserDatatableScope extends BaseDatatableScope
{
    /**
     * AppDatatableScope constructor.
     */
    public function __construct()
    {
        $this->setHtml([
            [
                'data' => 'name',
                'name' => 'name',
                'title' => 'Name',
            ],
            [
                'data' => 'email',
                'name' => 'email',
                'title' => 'Email',
            ],
            [
                'data' => 'is_verified',
                'name' => 'is_verified',
                'title' => 'Is Verified?'
            ],
            [
                'data' => 'birth_date',
                'name' => 'birth_date',
                'title' => 'Birth Date',
            ],
            [
                'data' => 'age',
                'name' => 'age',
                'title' => 'Age is 18th Up',
            ],
            [
                'data' => 'is_active',
                'name' => 'is_active',
                'title' => 'Is Active?',
                'searchable' => false,
                'orderable' => false
            ],
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function query()
    {
        /*$myDate = '1997-05-20';
        $years = Carbon::parse($myDate)->age;
        dd($years);*/

       $query = User::query()->where('is_verified', true);

        return datatables()->eloquent($query)
        ->editColumn('is_active', function ($model) {
                return $model->is_active ? 'YES' : 'NO';
            })
        ->editColumn('is_verified', function($model) {
            return $model->is_verified ? 'YES' : 'NO';
        })
        ->editColumn('birth_date', function ($model) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $model->birth_date)->format('Y-m-d');
        })
        ->editColumn('age', function ($model) {
            $bdayquery = Carbon::parse($model->birth_date)->age;
            if($bdayquery > 18)
            {
        return Carbon::parse($model->birth_date)->age.' '. 'true';
            }else{
               return Carbon::parse($model->birth_date)->age.' '.'false';
            }
        })
            ->addColumn('actions', function ($model) {
                return view('admin.shared.dtcontrols')
                    ->with('id', $model->getKey())
                    ->with('model', $model)
                    ->with('editUrl', route('users.edit', $model->getKey()))
                    ->with('deleteUrl', route('users.destroy', $model->getKey()))
                    ->render();
            })
            
           
            ->rawColumns(['actions'])
            ->make(true);
    }
}
