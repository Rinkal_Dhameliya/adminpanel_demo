<?php

namespace App\Domain\Admin\Datatables;

use App\Domain\Util\Datatables\BaseDatatableScope;
use Carbon\Carbon;
use App\Models\Book;

class BookDatatableScope extends BaseDatatableScope
{
    /**
     * AppDatatableScope constructor.
     */
    public function __construct()
    {
        $this->setHtml([
            [
                'data' => 'book_name',
                'name' => 'book_name',
                'title' => 'Book Name',
            ],
            [
                'data' => 'author_name',
                'name' => 'author_name',
                'title' => 'Author Name',
            ],
            [
                'data' => 'category',
                'name' => 'category_id',
                'title' => 'Category',
            ],
            [
                'data' => 'price',
                'name' => 'price',
                'title' => 'Price',
            ],
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function query()
    {
        $query = Book::query()->with(['category']);

        return datatables()->eloquent($query)
        ->editColumn('category', function ($model) {
                return $model->category->category_name;
            })
            ->addColumn('actions', function ($model) {              
                return view('admin.shared.dtcontrols')
                    ->with('id', $model->getKey())
                    ->with('model', $model)
                    ->with('editUrl', route('books.edit', $model->getKey()))
                    ->with('deleteUrl', route('books.destroy', $model->getKey()))
                    ->render();
            })
           
            ->rawColumns(['actions'])
            ->make(true);
    }
}