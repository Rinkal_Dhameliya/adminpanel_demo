<?php

namespace App\Domain\Util\Datatables;

use Yajra\Datatables\Html\Builder;

abstract class BaseDatatableScope
{
	protected $partialHtml;

	abstract public function query();

	public function html($url = null)
	{
		$columns = array_merge([
			[
				'data' => 'id',
				'name' => 'id',
				'title' => 'Id',
			],
		],
		$this->partialHtml,
		[
			[
				'data' => 'actions',
				'name' => 'actions',
				'title' => 'Action',
			],
		]);

		$builder = app('datatables.html');

		return $builder->columns($columns)
			->ajax($url ?: request()->fullurl());
	}

	public function setHtml(array $html)
	{
		$this->partialHtml = $html;

		return $this;
	}
}