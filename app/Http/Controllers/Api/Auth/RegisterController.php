<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\Api\Request\RegisterRequest;
use App\Models\User;
use App\Notifications\UserVerification;
use App\Http\Resources\UserResource;
use Symfony\Component\HttpFoundation\JsonResponse;

class RegisterController extends Controller
{
 		public function register(RegisterRequest $request)
 		{
 			$user = User::create($request->persist());
 			$user->notify(new UserVerification());	

 			return new UserResource($user);
 		}   

 		public function verifyAccount($code)
 		{
 			$user = User::where('email_verified_at', $code)->first();

 			if(!$user){
 				return new JsonResponse([
 					'success' => false,
 					'message' => 'Your link has been expired or used.'
 				]);
 			}

 			$user->fill([
 				'email_verified_at' => null,
 				'is_verified' => true
 			])->save();

 			return view('front.verified');
 		}
}
