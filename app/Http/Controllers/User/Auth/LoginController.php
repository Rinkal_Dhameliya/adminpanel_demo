<?php

namespace App\Http\Controllers\User\Auth;

//use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Domain\User\Request\LoginRequest;
//use App\Models\User;
use Socialite;

class LoginController extends Controller
{
    public function getLogin()
    {
    	return view('user.auth.login');
    }

    public function postLogin(LoginRequest $request)
    {  //dd(auth('web')->attempt(['email' =>    $request->get('email'), 'password' => $request->get('password')]));
    	if (auth('web')->attempt(['email' => $request->get('email'), 'password' => $request->get('password')])) {
            return redirect()->route('user:dashboard');
        }

    	session()->flash('error', 'Invalid Credentials!');
    	return redirect()->back()->withInput($request->all());
    }

    public function getLogout()
    {
    	auth('web')->logout();
    	return redirect()->route('user:login');
    }
}
