<?php

namespace App\Http\Controllers\User\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\User\Request\RegisterRequest;
use App\Models\User;
use App\Notifications\UserVerification;
use Carbon\Carbon;

class RegisterController extends Controller
{
    public function getRegister()
    {
    	return view('user.auth.register');
    }

    public function postRegister(RegisterRequest $request)
    {  
    	$user = User::create($request->persist());
    	$user->refresh();

        $user->notify(new UserVerification());
        session()->flash('success', 'Register Successfully! Now you can login & Email verify');
        return redirect(route('user:login'));
    }

    public function verifyAccount($code)
    {
        $user = User::where('verification_code', $code)->first();

        if(!$user){
            return new JsonResponse([
                'success' => false,
                'message' => 'Your link has been expired or used.'
            ]);
        }

        $user->fill([
            'verification_code' => null,
            'is_verified' => true
        ])->save();

        return view('front.verified');
    }
}