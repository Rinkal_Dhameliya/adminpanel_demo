<?php

namespace App\Http\Controllers\User\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Domain\Util\MailUtility;
use App\Domain\User\Request\EmailRequest;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;


class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showLinkRequestForm()
    {
        return view('user.auth.forgot-password');
    }

    public function sendResetLinkEmail(EmailRequest $request)
    {
    	$user = User::whereEmail($request->get('email'))->first();
    	$token = str_random(20). time();
    	$user->fill([
            'forgot_password_token' => $token
        ])->save();

         $data = [
            'name' => $user->name,
            'link' => route('user:reset:password', $token)
        ];
        (new MailUtility())->sendMail('user.emails.forgot-password', $data, $request->get('email'), 'User Forgot Password.');
        session()->flash('success', 'Reset password link sent to your email.');

        return redirect()->route('user:login');
    }
}