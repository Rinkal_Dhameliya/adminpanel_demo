<?php

namespace App\Http\Controllers\User\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\Api\Request\ResetPasswordRequest;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Models\User;

class ResetPasswordController extends Controller
{
     use ResetsPasswords;
    
    protected $redirectTo = '/home';

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showResetForm($token)
    {
    	$user = User::where('forgot_password_token', $token)->firstorfail();

    	return view('user.auth.reset-password', compact('user'));
    }

    public function reset(ResetPasswordRequest $request, $id)
    {
    	$user = User::findorfail($id);
    	$user->fill([
    		'password' => bcrypt($request->get('password'))
    	])->save();
    	session()->flash('success', 'Password updated successfully!.');

    	return redirect()->route('user:login');
    }
}
