<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\User\Datatables\BookDatatableScope;


class BookController extends Controller
{
    public function index(BookDatatableScope $datatable)
    {
    	 if (request()->ajax()) {
            return $datatable->query();
        }
    
    	return view('user.books.index', [
    		'html' => $datatable->html(),
    	]);
    }
}
