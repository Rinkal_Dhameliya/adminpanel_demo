<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Book;
use App\Models\User;
use Session;
use Stripe;
use App\Models\Charge;

class StripePaymentController extends Controller
{
    public function stripePost(Request $request)
    {
        Stripe::setApiKey(env('STRIPE_SECRET'));
 
        $token = request('stripeToken');
 
        $charge = Charge::create([
            'amount' => 1000,
            'currency' => 'usd',
            'description' => 'Test Book',
            'source' => $token,
        ]);
  
        Session::flash('success', 'Payment successful!');
          
       // return redirect()->back();

        $books = Book::query()->pluck('book_name','id');
        $users = User::query()->pluck('name','id');

        return view('user.issuebooks.add',compact('books', 'users'));
    }
}