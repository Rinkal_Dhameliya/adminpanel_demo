<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\User\Request\ProfileRequest;
use App\Models\User;

class ProfileController extends Controller
{
    public function index()
    {
    	$user = auth('web')->user();

    	return view('user.users.profile', compact('user'));
    }

    public function store(ProfileRequest $request)
    {
    	$user = auth('web')->user();
    	$user->fill($request->persist())->save();
    	session()->flash('success', 'User Profile updated successfully!.');

    	return back();
    }
}