<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Book;
use App\Models\IssueBook;
use Carbon\Carbon;
use App\Domain\User\Datatables\DashboardDatatableScope;

class DashboardController extends Controller
{
    public function index(DashboardDatatableScope $datatable)
    {
        if (request()->ajax()) {
            return $datatable->query();
        }

        $totalBooks = Book::count();

       // $query = auth()->user()->isbook()->with(['book','user'])->count();

        $total_issuebook =  auth()->user()->isbook()->with(['book','user'])->where('retn_book', '=', 'pending')->count();        

        $carbon = Carbon::today();
        $format = $carbon->format('Y-m-d');

        return view('user.dashboard.dashboard', compact('totalBooks', 'total_issuebook', 'format'),[
            'html' => $datatable->html()
        ]);
    }

    public function getPosts()
    {
        return \DataTables::of(auth()->user()->isbook()->with(['book','user'])->where('retn_book', '=', 'pending'))->make(true);
    }
}