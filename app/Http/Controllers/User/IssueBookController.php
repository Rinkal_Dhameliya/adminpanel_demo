<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\User\Datatables\IssueBookDatatableScope;
use App\Models\Book;
use App\Models\User;
use App\Models\IssueBook;
use App\Models\RequestIssueBook;
use App\Domain\User\Request\IssueBookRequest;

class IssueBookController extends Controller
{

	public function __construct()
    {
        $this->middleware('web');
    }

    public function index(IssueBookDatatableScope $datatable)
    {
    	 if (request()->ajax()) {
            return $datatable->query();
        }
    
    	return view('user.issuebooks.index', [
    		'html' => $datatable->html(),
    	]);
    }

    public function create()
    {
    	$books = Book::query()->pluck('book_name','id');
    	$users = User::query()->pluck('name','id');
    	return view('user.issuebooks.add',compact('books', 'users'));
    }

    public function store(IssueBookRequest $request)
    {   
    	$issue_book = IssueBook::create($request->persist());
        RequestIssueBook::create(['issuebook_id' => $issue_book->id,'status' => 'pending' ]);
        session()->flash('success', 'Pls Approve Your Book.');
        return redirect(route('issue-books.index'));
    }
}