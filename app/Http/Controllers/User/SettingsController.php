<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\User\Request\ChangePasswordRequest;

class SettingsController extends Controller
{
    public function userChangePassword()
	{
		return view('user.settings.change-password');
	}

	public function postUserChangePassword(ChangePasswordRequest $request)
	{
		$user = auth('web')->user();
		if(!auth()->attempt(['email' => $user->email, 'password' => $request->get('current_password')])) {
			session()->flash('error', 'Current password does not match!.');

			return back();
		}

		$user->fill([
			'password' => bcrypt($request->get('password'))
		])->save();
		session()->flash('success', 'Password changed successfully!.');

		return back();
	}
}
