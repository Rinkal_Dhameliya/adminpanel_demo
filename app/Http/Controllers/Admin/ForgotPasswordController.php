<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AdminUser;
//use App\Domain\Admin\Request\EmailRequest;
use App\Domain\Util\MailUtility;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showLinkRequestForm()
    {
        return view('admin.auth.forgot-password');
    }

    public function sendResetLinkEmail(Request $request)
    {
    	$user = AdminUser::whereEmail($request->get('email'))->first();
    	$token = str_random(20). time();
    	$user->fill([
            'forgot_password_token' => $token
        ])->save();

         $data = [
            'name' => $user->username,
            'link' => route('admin:reset:password', $token)
        ];
        (new MailUtility())->sendMail('admin.emails.forgot-password', $data, $request->get('email'), 'Admin Forgot Password.');
        session()->flash('success', 'Reset password link sent to your email.');

        return redirect()->route('admin:login');

    }
}
