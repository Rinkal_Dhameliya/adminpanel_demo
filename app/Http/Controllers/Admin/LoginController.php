<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\Admin\Request\LoginRequest;

class LoginController extends Controller
{
    public function getLogin()
    {  
    	return view('admin.auth.login');
    }

    public function postLogin(LoginRequest $request)
    {
    	if(auth('admin')->attempt(['email' => $request->get('email'), 'password' => $request->get('password')])){
    		return redirect()->route('admin:dashboard');
    	}

    	session()->flash('error', 'Invalid Credentials!');
    	return redirect()->back()->withInput($request->all());
    }

    public function getLogout()
    {
    	auth('admin')->logout();
    	return redirect()->route('admin:login');
    }
}