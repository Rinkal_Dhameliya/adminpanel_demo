<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Book;
use App\Models\User;
use App\Models\AdminUser;
use App\Models\IssueBook;
use App\Models\RequestIssueBook;
use App\Domain\Admin\Datatables\IssueBookDatatableScope;
use App\Domain\Admin\Request\IssueBookRequest;

class IssueBookController extends Controller
{
    public function index(IssueBookDatatableScope $datatable)
    {
    	 if (request()->ajax()) {
            return $datatable->query();
        }

    	return view('admin.issuebooks.index', [
    		'html' => $datatable->html(),
    	]);
    }

    public function create()
    {
    	$books = Book::query()->pluck('book_name','id');
    	$users = User::query()->pluck('name','id');
    	return view('admin.issuebooks.add',compact('books', 'users'));
    }

    public function store(IssueBookRequest $request)
    {
        $issue_book = IssueBook::create($request->persist());
        RequestIssueBook::create(['issuebook_id' => $issue_book->id,'status' => 'approved' ]);

        session()->flash('success', 'Issue Book added Successfully.');
        return redirect(route('issuebooks.index'));
    }

    public function edit($id)
    {	
    	$issuebook = IssueBook::findorfail($id);
    	$books = Book::query()->pluck('book_name','id');
    	$users = User::query()->pluck('name','id');

    	return view('admin.issuebooks.edit',compact('books', 'issuebook', 'users'));
    }	

    public function update(IssueBookRequest $request, $id)
    {	
        $issuebook = IssueBook::findorfail($id);
        $issuebook->fill($request->persist())->save();
        session()->flash("success", 'Issue Book Update Successfully.');

        return redirect()->route('issuebooks.index');
    }

    public function destroy($id)
    {	
    	$issuebook = IssueBook::findorfail($id);
        $issuebook->delete();
        session()->flash("success", 'Issue Book Deleted Successfully.');

        return redirect()->route('issuebooks.index');
    }
}