<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\Admin\Datatables\CategoryDatatableScope;
use App\Models\Category;
use App\Models\Book;
use App\Domain\Admin\Request\CategoryRequest;
use Validator;

class CategoryController extends Controller
{
    public function index(CategoryDatatableScope $datatable)
    {	
    	if (request()->ajax()) {
            return $datatable->query();
        }

    	return view('admin.categorys.index',[
    		'html' => $datatable->html(),
    	]);
    }

    public function store(CategoryRequest $request)
    {  
        Category::create($request->persist());
        
        return response()->json(['success'=>'Data is successfully added']);

        return redirect(route('categorys.index'))->with('success', ['your message,here']);
    }

    public function edit(Category $category)
    {
        return view('admin.categorys.edit', compact('category'));
    }

    public function update(CategoryRequest $request, $id)
    {
        $category = Category::findorfail($id);
        $category->fill($request->persist())->save();
        session()->flash("success", 'Category Update Successfully.');

        return redirect()->route('categorys.index');
    }

    public function destroy(Category $category)
    {
        $category->delete();
        session()->flash("success", 'Category Deleted Successfully.');

        return redirect()->route('categorys.index');
    }
}