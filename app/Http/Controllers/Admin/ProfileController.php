<?php

namespace App\Http\Controllers\Admin;

//use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\Admin\Request\ProfileRequest;
//use App\Models\AdminUser;

class ProfileController extends Controller
{
    public function index()
    {
    	$user = auth('admin')->user();

    	return view('admin.users.profile', compact('user'));
    }

    public function store(ProfileRequest $request)
    {
    	$user = auth('admin')->user();
    	$user->fill($request->persist())->save();
    	session()->flash('success', 'Profile updated successfully!.');

    	return back();
    }
}
