<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\Admin\Request\ChangePasswordRequest;

class SettingsController extends Controller
{
	public function adminChangePassword()
	{
		return view('admin.settings.change-password');
	}

	public function postAdminChangePassword(ChangePasswordRequest $request)
	{
		$user = auth('admin')->user();
		if(!auth()->attempt(['email' => $user->email, 'password' => $request->get('current_password')])) {
			session()->flash('error', 'Current password does not match!.');

			return back();
		}

		$user->fill([
			'password' => bcrypt($request->get('password'))
		])->save();
		session()->flash('success', 'Password changed successfully!.');

		return back();
	}
}