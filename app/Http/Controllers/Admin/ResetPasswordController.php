<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\Api\Request\ResetPasswordRequest;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Models\AdminUser;

class ResetPasswordController extends Controller
{
    use ResetsPasswords;

    protected $redirectTo = '/home';

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showResetForm($token)
    {
    	$user = AdminUser::where('forgot_password_token', $token)->firstorfail();

    	return view('admin.auth.reset-password', compact('user'));
    }

    public function reset(ResetPasswordRequest $request, $id)
    {
    	$user = AdminUser::findorfail($id);
    	$user->fill([
    		'password' => bcrypt($request->get('password'))
    	])->save();
    	session()->flash('success', 'Password updated successfully!.');

    	return redirect()->route('admin:login');
    }
}
