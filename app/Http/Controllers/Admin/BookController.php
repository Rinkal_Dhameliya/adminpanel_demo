<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\Admin\Datatables\BookDatatableScope;
use App\Domain\Admin\Request\BookRequest;
use App\Models\Book;
use App\Models\Category;

class BookController extends Controller
{
    public function index(BookDatatableScope $datatable)
    {
    	 if (request()->ajax()) {
            return $datatable->query();
        }
    
    	return view('admin.books.index', [
    		'html' => $datatable->html(),
    	]);
    }

    public function create()
    {
    	$categorys = Category::query()->pluck('category_name','id');
    	return view('admin.books.add',compact('categorys'));
    }

    public function store(BookRequest $request)
    {
    	Book::create($request->persist());
        session()->flash('success', 'Book added Successfully.');
        return redirect(route('books.index'));
    }

    public function edit(Book $book)
    {	
    	$categorys = Category::query()->pluck('category_name','id');
    	return view('admin.books.edit',compact('categorys', 'book'));
    }	

    public function update(BookRequest $request, $id)
    {
        $book = Book::findorfail($id);
        $book->fill($request->persist())->save();
        session()->flash("success", 'Book Update Successfully.');

        return redirect()->route('books.index');
    }

    public function destroy(Book $book)
    {
        $book->delete();
        session()->flash("success", 'Book Deleted Successfully.');

        return redirect()->route('books.index');
    }
}