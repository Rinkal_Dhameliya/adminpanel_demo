<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Book;
use App\Models\IssueBook;
use App\Models\RequestIssueBook;
use Carbon\Carbon;
use App\Domain\Admin\Datatables\DashboardDatatableScope;
use App\Notifications\UserVerification;
use Symfony\Component\HttpFoundation\JsonResponse;
use DataTables;

class DashboardController extends Controller
{
    public function index(DashboardDatatableScope $datatable)
    {
        if (request()->ajax()) {
            return $datatable->query();
        }
            
        $query = User::query()->where('is_verified', true);
        $totalUsers = $query->count();
            
        $totalBooks = Book::count();

        $bookquery = RequestIssueBook::query()->where('status', '=', 'approved');
        //retn_book -> panding;

       // $bookq = IssueBook::query()->where('retn_book', '=', 'panding');

        $totalIssueBooks = $bookquery->count();

        $carbon = Carbon::today();
        $format = $carbon->format('Y-m-d');

        $htmldata = $datatable->html();

        return view('admin.dashboard.dashboard', compact('totalBooks','totalUsers', 'totalIssueBooks', 'format','htmldata'));
    }

    public function updateBook(Request $request){
        $requestUser = IssueBook::query()->find($request->id);
        $requestUser->retn_book = "returned";
        if($requestUser->save()){
           session()->flash('success', 'Your Book Return Successfully.');
            return new JsonResponse([
                'success' => true
                ]);
        }
        session()->flash('success', 'Your Book not Return Successfully.');
        return new JsonResponse([
            'success' => false
            ]);
    }

    public function getPosts()
    {
        return \DataTables::of(IssueBook::query()->whereDate('expiry_date','=',Carbon::now())->where('retn_book', '=', 'returned')->with('book','user'))->make(true);
    }
}