<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\Admin\Datatables\IssueBookRequestDatatableScope;
use App\Domain\Admin\Request\IssueBookRequest;
use App\Models\RequestIssueBook;
use App\Models\IssueBook;
use App\Models\Book;
use App\Models\User;
use App\Domain\Admin\Request\UpdateStatusRequest;
use Symfony\Component\HttpFoundation\JsonResponse;

class IssueBookRequestController extends Controller
{
    public function index(IssueBookRequestDatatableScope $datatable)
    {
    	 if (request()->ajax()) {
            return $datatable->query();
        }

    	return view('admin.issuebookrequest.index', [
    		'html' => $datatable->html(),
    	]);
    }

    public function updateStatus(Request $request){
        $requestUser = RequestIssueBook::query()->find($request->issuebook_id);

        $requestUser->status= "approved";
        
        if($requestUser->save()){
           session()->flash('success', 'Your Book Approved Successfully.');
            return new JsonResponse([
                'success' => true
                ]);
        }
        session()->flash('success', 'Your Book not Approved Successfully.');
        return new JsonResponse([
            'success' => false
            ]);
    }

    public function edit($id)
    {   
        $issuebook = RequestIssueBook::findorfail($id);
        $books = Book::query()->pluck('book_name','id');
        $users = User::query()->pluck('name','id');

        return view('admin.issuebookrequest.edit',compact('books', 'issuebook', 'users'));
    }   

    public function update(IssueBookRequest $request, $id)
    {   
        $issuebook = RequestIssueBook::findorfail($id);

        $issuebook->bookreturn->fill($request->persist())->save();

        session()->flash("success", 'User Request Update Successfully.');

        return redirect()->route('issuebook-request.index');
    }

    public function destroy($id)
    {   
        $issuebook = RequestIssueBook::findorfail($id);
        $issuebook->delete();
        session()->flash("success", 'User Request Deleted Successfully.');

        return redirect()->route('issuebook-request.index');
    }
}