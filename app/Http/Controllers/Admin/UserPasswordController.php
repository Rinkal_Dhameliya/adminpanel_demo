<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Domain\Admin\Request\UserPasswordRequest;

class UserPasswordController extends Controller
{
     use ResetsPasswords;
    
    protected $redirectTo = '/home';

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showResetForm($token)
    {
    	$user = User::where('user_password_token', $token)->firstorfail();

    	return view('admin.auth.user-password', compact('user'));
    }

    public function reset(UserPasswordRequest $request, $id)
    {
    	$user = User::findorfail($id);
    	$user->fill([
    		'password' => bcrypt($request->get('password'))
    	])->save();
    	session()->flash('success', 'Password updated successfully!.');

    	return redirect()->route('user:login');
    }
}
