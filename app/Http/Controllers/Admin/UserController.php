<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\Admin\Datatables\UserDatatableScope;
use App\Models\User;
use App\Domain\Admin\Request\UpdateUserRequest;
use App\Domain\Admin\Request\UserRequest;
use App\Domain\Util\MailUtility;
use Carbon\Carbon;

class UserController extends Controller
{
    public function index(UserDatatableScope $datatable)
    {
    	if (request()->ajax()) {
            return $datatable->query();
        }

    	return view('admin.users.index', [
    		'html' => $datatable->html(),
    	]);
    }

    public function create()
    {
        return view('admin.users.add');
    }

    public function store(UserRequest $request)
    {  
        $user = User::create($request->persist());
        
        $token = str_random(20). time();

        if($user->fill([
            'user_password_token' => $token
        ])->save()){

        $data = [
            'name' => $user->name,
            'link' => route('admin:user:password', $token)
        ];
        (new MailUtility())->sendMail('admin.emails.user-password', $data, $request->get('email'), 'Your Pasword.');
        session()->flash('success', 'password link sent to your email.');

        session()->flash('success', 'User Added Successfully.');
        return redirect(route('users.index'));
        }
    }

    public function edit(User $user)
    {
    	return view('admin.users.edit', compact('user'));
    }

    public function update(UpdateUserRequest $request, $id)
    {
        $user = User::findorfail($id);
        $user->fill($request->persist())->save();
        session()->flash('success', 'User Updated Successfully.');

        return redirect()->route('users.index');
    }

    public function destroy(User $user)
    {
        $user->delete();
        session()->flash('success', 'User Deleted Successfully.');

        return redirect()->route('users.index');
    }
}