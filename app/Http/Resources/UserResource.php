<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       return [
            'id' => $this->getkey(),
            'name' => $this->name,
            'email' => $this->email,

       ];
    }

    public function with($request)
    {
        return array_merge([
            'success' => true
        ]);
    }
}
