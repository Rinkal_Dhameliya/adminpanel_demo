<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\IssueBook;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class ChangeIssueBook extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'change:issue:book';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $currentTime = Carbon::now();
        $items = IssueBook::query()->whereDate('expiry_date', $currentTime->addDays(1))->with(['user'])->get();

        foreach($items as $item)
        {
            Mail::raw("Tomorrow Last Date of Renew book", function($message) use ($item){
               $message->from('info@admin.com');
               $message->to($item->user->email)->subject('Renew Book');
            });
        }
        $this->info('Send Mail Successfully');
    }
}