<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IssueBook extends Model
{
    protected $table = 'issue_books';

    protected $fillable = [
    	'user_id', 'book_id', 'issue_date', 'expiry_date', 'retn_book'
    ];

    public function book(){
    	return $this->belongsTo(Book::class,'book_id','id');
    }

    public function user(){
    	return $this->belongsTo(User::class,'user_id','id');
    }

    public function bookrequests(){
        return $this->hasMany(RequestIssueBook::class);
    }
}