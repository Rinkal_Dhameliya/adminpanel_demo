<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'books';

    protected $fillable = [
    	'category_id', 'book_name', 'author_name', 'price'
    ];

    public function category(){
    	return $this->belongsTo(Category::class,'category_id','id');
    }

    public function issue_book(){
 		return $this->hasMany(IssueBook::class);
 	}



}
