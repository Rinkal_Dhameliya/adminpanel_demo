<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class AdminUser extends Authenticatable
{
 	protected $fillable = [
 		'username', 'email', 'password', 'forgot_password_token'
 	];   
}