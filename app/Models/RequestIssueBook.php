<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RequestIssueBook extends Model
{
    protected $table = 'issue_book_request';

    protected $fillable = [
    	'issuebook_id', 'status'
    ];

    public function bookreturn(){
    	return $this->belongsTo(IssueBook::class, 'issuebook_id', 'id');
    }
}