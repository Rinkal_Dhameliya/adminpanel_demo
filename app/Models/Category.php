<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $table = 'categorys';

    protected $fillable = [
 		'category_name', 'is_active'
 	]; 

 	public function books(){
 		return $this->hasMany(Book::class);
 	}
}